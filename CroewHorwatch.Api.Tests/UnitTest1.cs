﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CroweHorwath.Controllers;

namespace CroewHorwatch.Api.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetReturnsHelloWorld()
        {
            // Normally, a Service layer would be injected into the Controller and the Service would be unit tested for any business logic as well.  For simplicity, test return of Controller here.
            var controller = new SampleController();
            
            var response = controller.Get();
            
            // if a repository pattern is used, then use MOQ to mock the return of the repository.

            Assert.AreEqual("Hello World!", response.MessageText);
        }
    }
}

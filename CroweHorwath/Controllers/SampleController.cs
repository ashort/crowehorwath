﻿using CroweHorwath.Model;
using System.Web.Http;

namespace CroweHorwath.Controllers
{
    public class SampleController : ApiController
    {
        public SampleData Get()
        {
            // TODO: Create repository pattern to return text from database?
            return new SampleData() { MessageText = "Hello World!" };
        }
    }
}

﻿using CroweHorwath.Model;
using NLog;
using System.Configuration;

namespace CroweHorwatch.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            string baseUrl = ConfigurationManager.AppSettings["baseURL"];
            SampleApi api = new SampleApi(baseUrl);
            SampleData helloWorld = api.Get();
            LogManager.GetCurrentClassLogger().Log(LogLevel.Trace, helloWorld.MessageText);
            
            System.Console.ReadLine();
        }
    }
}

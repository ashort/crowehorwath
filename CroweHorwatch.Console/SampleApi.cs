﻿using CroweHorwath.Model;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;

namespace CroweHorwatch.Console
{
    public class SampleApi
    {
        private string _url;

        private static readonly string SAMPLE_ROUTE = "/api/Sample";

        public SampleApi(string url)
        {
            _url = url;
        }

        public SampleData Get()
        {
            WebRequest wrGETURL;
            
            wrGETURL = WebRequest.Create(_url + SAMPLE_ROUTE);

            Stream objStream;
            objStream = wrGETURL.GetResponse().GetResponseStream();

            StreamReader objReader = new StreamReader(objStream);
            string result = objReader.ReadToEnd();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            return jss.Deserialize<SampleData>(result);
        }
    }
}
